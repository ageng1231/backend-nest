import { Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './infrastructures/configs/prisma/prisma.module';
import { LoggerModule } from './infrastructures/logger/logger.module';
import { DatabaseModule } from './infrastructures/database/database.module';
import { HttpModule } from './infrastructures/http/http.module';
import { HitungTransaksiUseCase } from './application/use_case/accounting/hitung_transaksi_use_case';

@Module({
  imports: [PrismaModule, LoggerModule, DatabaseModule, HttpModule],
  controllers: [AppController],
  providers: [AppService, Logger],
})
export class AppModule {}
