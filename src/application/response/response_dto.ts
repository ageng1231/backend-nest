import { HttpStatus } from "@nestjs/common";

export class SuccessResponse {
    message: string;
    success: boolean = true;
    data: any[] = [];
    status: HttpStatus;
    constructor({message = '', status = HttpStatus.OK,data = []}){
        this.message = message;
        this.status = status;
        this.data = data;
    }
}
export class ErrorResponse {
    message: string;
    success: boolean = false;
    data: any[] = [];
    status: HttpStatus;
    constructor({message = '', status = HttpStatus.BAD_GATEWAY,data = []}){
        this.message = message;
        this.status = status;
        this.data = data;
    }
}