import { Injectable } from '@nestjs/common';
import { UseCase } from '../use_case';
import { AccountingRepository } from '@app/application/repositories/accounting_repository';

export interface IHitungTransaksiUseCaseRequest {
  data: any[];
}

@Injectable()
export class HitungTransaksiUseCase
  implements UseCase<IHitungTransaksiUseCaseRequest, any>
{
  constructor(private readonly repos: AccountingRepository) {}
  async execute(request: IHitungTransaksiUseCaseRequest): Promise<any> {
    const saveData = await this.repos.hitungTransaksi(request);
    return saveData;
  }
}
