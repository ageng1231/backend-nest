import { PrismaAccountingRepository } from '@app/infrastructures/database/repositories/prisma_accounting_repository';
import { HitungTransaksiUseCase } from './hitung_transaksi_use_case';
import { DatabaseService } from '@app/infrastructures/database/database.service';
import { faker } from '@faker-js/faker';
import { randomUUID } from 'crypto';
const prisma = new DatabaseService();
const concurrentArrays = [...Array(1000).keys()];
const allHeader: any[] = [];
const allAcuan: any[] = [];
const acuanParams: any = {
  tanggal_awal: new Date('2024-04-23'),
  tanggal_akhir: new Date('2024-08-23'),
  nominal: 100000,
};
const params: any[] = [
  {
    tanggal_awal: new Date('2024-04-23'),
    tanggal_akhir: new Date('2024-04-31'),
    nominal: 10000,
  },
  {
    tanggal_awal: new Date('2024-05-01'),
    tanggal_akhir: new Date('2024-05-31'),
    nominal: 20000,
  },
  {
    tanggal_awal: new Date('2024-06-01'),
    tanggal_akhir: new Date('2024-06-30'),
    nominal: 20000,
  },
  {
    tanggal_awal: new Date('2024-07-01'),
    tanggal_akhir: new Date('2024-07-31'),
    nominal: 20000,
  },
  {
    tanggal_awal: new Date('2024-08-01'),
    tanggal_akhir: new Date('2024-08-23'),
    nominal: 30000,
  },
];
let acuanParams2: any = {
  tanggal_awal: new Date('2024-04-23'),
  tanggal_akhir: new Date('2024-06-02'),
  nominal: 60000,
};
const paramsStatic2: any[] = [
  {
    tanggal_awal: new Date('2024-04-23'),
    tanggal_akhir: new Date('2024-04-31'),
    nominal: 20000,
  },
  {
    tanggal_awal: new Date('2024-05-01'),
    tanggal_akhir: new Date('2024-05-31'),
    nominal: 20000,
  },
  {
    tanggal_awal: new Date('2024-06-01'),
    tanggal_akhir: new Date('2024-06-02'),
    nominal: 20000,
  },
];
function createRandomTransactions() {
  const dateAwal = faker.date.anytime();
  return {
    tanggal_awal: faker.date.anytime(),
    tanggal_akhir: new Date(dateAwal.setDate(dateAwal.getDate() + 15)),
    nominal: faker.number.float(),
  };
}
describe('HitungTransaksi', () => {
  it('should count transactional data', async () => {
    const repo = new PrismaAccountingRepository(prisma);
    const hitungTransaksiLogic = new HitungTransaksiUseCase(repo);
    const id = randomUUID().toString();
    allAcuan[id] = acuanParams;
    const hitung = await hitungTransaksiLogic.execute({ data: params });
    delete hitung.id;
    allHeader[id] = hitung;
  });
  it('should count transactional data number 2', async () => {
    const repo = new PrismaAccountingRepository(prisma);
    const hitungTransaksiLogic = new HitungTransaksiUseCase(repo);
    const id = randomUUID().toString();

    allAcuan[id] = acuanParams2;
    const hitung = await hitungTransaksiLogic.execute({ data: paramsStatic2 });
    delete hitung.id;
    allHeader[id] = hitung;
  });
  test.concurrent.each(concurrentArrays)(
    'concurrent-hitung-transaksi(%i, %i)',
    async () => {
      const id = randomUUID().toString();
      const repo = new PrismaAccountingRepository(prisma);
      const hitungTransaksiLogic = new HitungTransaksiUseCase(repo);
      const params = await faker.helpers.multiple(createRandomTransactions, {
        count: 10,
      });
      let nominalAcuan = 0;
      const firstHeader = params.sort(
        (x: any, y: any) =>
          new Date(x.tanggal_awal).getTime() -
          new Date(y.tanggal_awal).getTime(),
      );
      params.map((i: any) => {
        nominalAcuan += i.nominal;
      });
      const sortTglAkhir = params.sort(
        (x: any, y: any) =>
          new Date(y.tanggal_akhir).getTime() -
          new Date(x.tanggal_akhir).getTime(),
      );

      acuanParams2 = {
        tanggal_awal: firstHeader[0].tanggal_awal,
        tanggal_akhir: sortTglAkhir[0].tanggal_akhir,
        nominal: Math.ceil(nominalAcuan),
      };
      const hitung = await hitungTransaksiLogic.execute({ data: params });
      delete hitung.id;
      allHeader[id] = hitung;
      allAcuan[id] = acuanParams2;
    },
    1000,
  );
  
  test('check-count-header', async () => {
    const count = await prisma.accountant_headers.count();
    expect(count).toBe(concurrentArrays.length + 2);
  });
  test('check-count-trx', async () => {
    const countTrx = await prisma.accountant_transactions.count();
    expect(countTrx).toBe(concurrentArrays.length * 10 + 8);
  });
});
