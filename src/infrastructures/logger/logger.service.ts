import { Injectable, Logger } from '@nestjs/common';
import { ILogger } from './Logger.interface';

@Injectable()
export class LoggerService extends Logger implements ILogger {
  debug(context: string, message: string) {
    super.debug('[DEBUG] ' + message, context);
  }
  log(context: string, message: string) {
    super.debug('[DEBUG] ' + message, context);
  }
  error(context: string, message: string) {
    super.debug('[DEBUG] ' + message, context);
  }
  warn(context: string, message: string) {
    super.debug('[DEBUG] ' + message, context);
  }
  verbose(context: string, message: string) {
    if (process.env.NODE_ENV !== 'production') {
      super.debug('[DEBUG] ' + message, context);
    }
  }
}
