import { AccountingRepository } from '@app/application/repositories/accounting_repository';
import { Injectable } from '@nestjs/common';
import { DatabaseService } from '../database.service';
import { randomUUID } from 'crypto';

@Injectable()
export class PrismaAccountingRepository implements AccountingRepository {
  constructor(private readonly db: DatabaseService) {}
  async hitungTransaksi(request: any): Promise<any> {
    request.data = request.data.sort(
      (x: any, y: any) =>
        new Date(x.tanggal_awal).getTime() - new Date(y.tanggal_awal).getTime(),
    );
    const tanggalAwalheader = request.data[0].tanggal_awal;
    const sortTglAkhir = request.data.sort(
      (x: any, y: any) =>
        new Date(y.tanggal_akhir).getTime() -
        new Date(x.tanggal_akhir).getTime(),
    );
    const tanggalAkhirHeader = sortTglAkhir[0].tanggal_akhir;
    const paramsheader = {
      tanggal_awal: new Date(tanggalAwalheader),
      tanggal_akhir: new Date(tanggalAkhirHeader),
      id: randomUUID().toString(),
      nominal: 0,
    };
    for (const v in request.data) {
      const i = request.data[v];
      const output = i.nominal;
      paramsheader.nominal += output;
      const row = {
        ...i,
        tanggal_awal: new Date(i.tanggal_awal),
        tanggal_akhir: new Date(i.tanggal_akhir),
        nominal: parseFloat(paramsheader.nominal.toFixed(2)),
        header_id: paramsheader.id,
      };
      await this.db.accountant_transactions.upsert({
        where: {
          id: row.id || '',
        },
        create: { ...row, id: randomUUID().toString() },
        update: row,
      });
    }
    paramsheader.nominal = Math.ceil(paramsheader.nominal);
    await this.db.accountant_headers.create({
      data: paramsheader
    });
    return paramsheader;
  }
}
