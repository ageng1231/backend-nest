import { Module } from '@nestjs/common';
import { DatabaseService } from './database.service';
import { AccountingRepository } from '@app/application/repositories/accounting_repository';
import { PrismaAccountingRepository } from './repositories/prisma_accounting_repository';

@Module({
  providers: [
    DatabaseService,
    {
      provide: AccountingRepository,
      useClass: PrismaAccountingRepository,
    },
  ],
  exports: [
    {
      provide: AccountingRepository,
      useClass: PrismaAccountingRepository,
    },
  ],
})
export class DatabaseModule {}
