import { AccountingRepository } from '@app/application/repositories/accounting_repository';

export class InMemoryAccountingRepository implements AccountingRepository {
  async hitungTransaksi(request: any): Promise<number> {
    console.warn(request);
    return 0;
  }
}
