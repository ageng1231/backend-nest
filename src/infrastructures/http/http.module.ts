import { Module } from '@nestjs/common';
import { PostingTransaksiController } from './accounting/posting_transaksi/posting_transaksi.controller';
import { HitungTransaksiUseCase } from '@app/application/use_case/accounting/hitung_transaksi_use_case';
import { AppModule } from '@app/app.module';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [PostingTransaksiController]
})
export class HttpModule {}
