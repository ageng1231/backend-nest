import { AccountingRepository } from '@app/application/repositories/accounting_repository';
import { ErrorResponse, SuccessResponse } from '@app/application/response/response_dto';
import { HitungTransaksiUseCase } from '@app/application/use_case/accounting/hitung_transaksi_use_case';
import { BadRequestException, Body, Catch, Controller, HttpException, HttpStatus, NotFoundException, Post, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller('posting-transaksi')
export class PostingTransaksiController {
    constructor(
        private readonly repos: AccountingRepository
    ){}
    @Post('')
    async execute(@Body() request:any, @Res() res: Response){
        try{
            let logicalUnit = new HitungTransaksiUseCase(this.repos)
            let response = new SuccessResponse({
                message: 'Sukses! Posting Transaksi',
                data: request.data,
                status: HttpStatus.ACCEPTED
            })
            console.warn(request);
            response.data = await logicalUnit.execute(request)
            if(request?.data == null || request?.data == undefined){
                throw new BadRequestException("Request Must Not Be Null")
            }
            if(request.data.length < 1){
                throw new BadRequestException("Request Must Be Array")
            }
            logicalUnit.execute(request);
            return res.status(response.status).json(response);
        }catch(ex: any){
            let response = new ErrorResponse({
                message: ex.message,
                status: ex?.status ? ex.status : HttpStatus.BAD_GATEWAY
            })
            return res.status(response.status).json(response);
        }
        
    }
    
}
