import { Test, TestingModule } from '@nestjs/testing';
import { PostingTransaksiController } from './posting_transaksi.controller';
import { AccountingRepository } from '@app/application/repositories/accounting_repository';
import { AppModule } from '@app/app.module';
import { DatabaseModule } from '@app/infrastructures/database/database.module';
import { HttpModule } from '../../http.module';
import axios from "axios";
describe('PostingTransaksiController', () => {
  let controller: PostingTransaksiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[AppModule, DatabaseModule, HttpModule],
      controllers: [PostingTransaksiController],
    }).compile();

    controller = module.get<PostingTransaksiController>(PostingTransaksiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  it("mock-axios-request", () => {
    jest.mock("axios");
    jest.mocked(axios).post('http://localhost:3000/posting-transaksi', {
      data: [
          {
              "tanggal_awal": "2024-03-04",
              "tanggal_akhir": "2024-04-31",
              "nominal": 10000
          },
          {
              "tanggal_awal": "2024-05-01",
              "tanggal_akhir": "2024-05-31",
              "nominal": 20000
          },
          {
              "tanggal_awal": "2024-06-01",
              "tanggal_akhir": "2024-06-30",
              "nominal": 20000
          },
          {
              "tanggal_awal": "2024-07-01",
              "tanggal_akhir": "2024-07-31",
              "nominal": 20000
          },
          {
              "tanggal_awal": "2024-08-01",
              "tanggal_akhir": "2024-08-23",
              "nominal": 30000
          }
      ]
    });
  })
  
  const concurrentArrays = [...Array(1000).keys()];
  test.concurrent.each(concurrentArrays)("concurrent-axios-request", () => {
    jest.mock("axios");
    jest.mocked(axios).post('http://localhost:3000/posting-transaksi', {
      data: [
          {
              "tanggal_awal": "2024-03-04",
              "tanggal_akhir": "2024-04-31",
              "nominal": 10000
          },
          {
              "tanggal_awal": "2024-05-01",
              "tanggal_akhir": "2024-05-31",
              "nominal": 20000
          },
          {
              "tanggal_awal": "2024-06-01",
              "tanggal_akhir": "2024-06-30",
              "nominal": 20000
          },
          {
              "tanggal_awal": "2024-07-01",
              "tanggal_akhir": "2024-07-31",
              "nominal": 20000
          },
          {
              "tanggal_awal": "2024-08-01",
              "tanggal_akhir": "2024-08-23",
              "nominal": 30000
          }
      ]
    });
  })  
});
